package org.rzayneev.active.link

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.GroovyPageUnitTestMixin} for usage instructions
 */
@TestFor(ActiveLinkTagLib)
class ActiveLinkTagLibSpec extends Specification {
    
    def linkAttrsTestList = [controller: 'test', action: 'list', params: [page: 1]]


    def setup() {
    }

    def cleanup() {
    }

    void "check simple active link"() {
        given: 'request uri is /test/list/page'
        request.forwardURI = "/test/list/page"
        expect:
        applyTemplate('<g:activeLink controller="demo">Demo</g:activeLink>') == applyTemplate('<g:link controller="demo">Demo</g:link>')
        applyTemplate('<g:activeLink controller="test">Test</g:activeLink>') == applyTemplate('<g:link controller="test" class="active">Test</g:link>')
    }

    void "with custom activeClass"() {
        given: 'request uri is /test/list/page'
        request.forwardURI = "/test/list/page"
        expect:
        applyTemplate('<g:activeLink controller="test" activeClass="enabled">Test</g:activeLink>') == applyTemplate('<g:link controller="test" class="enabled">Test</g:link>')
    }

    void "with custom conditions"() {
        given: 'request uri is /test/list?page=1&sort=asc'
        request.forwardURI = "/test/list?page=1&sort=asc"
        params.controller="test"
        params.action="list"
        params.page=1
        params.sort='asc'
        expect:
        applyTemplate('<g:activeLink controller="test" condition="${~/test/}">Test</g:activeLink>') == applyTemplate('<g:link controller="test" class="active">Test</g:link>')
        applyTemplate('<g:activeLink controller="demo" condition="${true}">Test</g:activeLink>') == applyTemplate('<g:link controller="demo" class="active">Test</g:link>')
        applyTemplate('<g:activeLink controller="test" condition="${false}">Test</g:activeLink>') == applyTemplate('<g:link controller="test">Test</g:link>')
        applyTemplate("<g:activeLink controller=\"test\" condition=\"[controller: ['test']]\">Test</g:activeLink>") == applyTemplate('<g:link controller="test" class="active">Test</g:link>')
        applyTemplate('<g:activeLink controller="test" action="list" condition="exclude">Test</g:activeLink>') == applyTemplate('<g:link controller="test" action="list" class="active">Test</g:link>')
        applyTemplate("<g:activeLink controller=\"test\" action=\"list\" params=\"[sort: 'asc', page:1]\" condition=\"include\">Test</g:activeLink>") == applyTemplate("<g:link controller=\"test\" action=\"list\" params=\"[sort: 'asc', page:1]\" class=\"active\">Test</g:link>")
    }


    void "with additional link class"() {
        given: 'request uri is /test/list?page=1&sort=asc'
        request.forwardURI = "/test/list?page=1&sort=asc"
        expect:
        applyTemplate('<g:activeLink controller="test" class="link">Test</g:activeLink>') == applyTemplate('<g:link controller="test" class="link active">Test</g:link>')
        applyTemplate('<g:activeLink controller="demo" class="link">Demo</g:activeLink>') == applyTemplate('<g:link controller="demo" class="link">Demo</g:link>')
    }

    void "with wrapper and additional link class"() {
        given: 'request uri is /test/list?page=1&sort=asc'
        request.forwardURI = "/test/list?page=1&sort=asc"
        expect:
        applyTemplate('<g:activeLink controller="test" class="link" wrapper="li">Test</g:activeLink>') == applyTemplate("<li class=\"active\"><g:link controller=\"test\" class=\"link\">Test</g:link></li>")
        applyTemplate('<g:activeLink controller="demo" class="link" wrapper="li">Demo</g:activeLink>') == applyTemplate("<li><g:link controller=\"demo\" class=\"link\">Demo</g:link></li>")
    }

    void "with wrapper and custom activeClass"() {
        given: 'request uri is /test/list?page=1&sort=asc'
        request.forwardURI = "/test/list?page=1&sort=asc"
        expect:
        applyTemplate('<g:activeLink controller="test" class="link" activeClass="current" wrapper="li">Test</g:activeLink>') == applyTemplate("<li class=\"current\"><g:link controller=\"test\" class=\"link\">Test</g:link></li>")
    }

    void "check active for RegExp condition"() {
        when: 'request is /test/list/page'
        request.forwardURI = "/test/list/page"
        then:
        tagLib.isLinkActiveFor(createTestLinkUri(linkAttrsTestList), ~/(\w)+\/(\w)+\/(\w)+/)
        !tagLib.isLinkActiveFor(createTestLinkUri(linkAttrsTestList), ~/(\d)+\/(\d)+\/(\d)+/)
        tagLib.isLinkActiveFor(createTestLinkUri(linkAttrsTestList), ~/page/)
    }

    void "check active for Map condition"() {
        when: "request uri /test/list?page=1"
        params.controller = 'test'
        params.action = 'list'
        params.page = 1
        then: "active for combination [controller: [test]] [action: [list]] [page: 1]"
        tagLib.isLinkActiveFor(createTestLinkUri(linkAttrsTestList), [controller: 'test'])
        tagLib.isLinkActiveFor(createTestLinkUri(linkAttrsTestList), [controller: ['demo', 'test']])
        tagLib.isLinkActiveFor(createTestLinkUri(linkAttrsTestList), [action: 'list'])
        tagLib.isLinkActiveFor(createTestLinkUri(linkAttrsTestList), [controller: 'test', action: 'list', page: 1])

        when: "request uri /test/list?page=1"
        params.controller = 'test'
        params.action = 'list'
        params.page = 1
        then: "Not active if some part not same"
        !tagLib.isLinkActiveFor(createTestLinkUri(linkAttrsTestList), [controller: ['testing'], action: 'list', page: 1])
    }

    void "check active for boolean condition"() {
        assertTrue(tagLib.isLinkActiveFor(createTestLinkUri(linkAttrsTestList), true))
        assertFalse(tagLib.isLinkActiveFor(createTestLinkUri(linkAttrsTestList), false))
    }

    void "check active for exclude get params condition"() {
        when: "request uri is 'test/list?param=demo'"
        request.forwardURI = "/test/list?param=demo"
        then: "active for '/test/list' && '/test/list?param=alfa'"
        tagLib.isLinkActiveFor(createTestLinkUri([controller: 'test', action: 'list']), 'exclude')
        tagLib.isLinkActiveFor(createTestLinkUri([uri: '/test/list?param=alfa']), 'exclude')

        when: "request uri is 'test/list?param=demo'"
        request.forwardURI = "/test/list?param=demo"
        then: "Not active for '/test' && '/test/show/' && '/test/list/demo'"
        !tagLib.isLinkActiveFor(createTestLinkUri([controller: 'test']), 'exclude')
        !tagLib.isLinkActiveFor(createTestLinkUri([controller: 'test', action: 'show']), 'exclude')
        !tagLib.isLinkActiveFor(createTestLinkUri([uri: '/test/list/demo']), 'exclude')
    }


    void "check active for default or empty condition"() {

        when: "request uri is 'test/list'"
        request.forwardURI = "/test/list?page=2"
        then: "active for '/test' && '/test?param=demo' && '/test/list/' && '/test/list?param=demo'"
        tagLib.isLinkActiveFor(createTestLinkUri([controller: 'test']), 'default')
        tagLib.isLinkActiveFor(createTestLinkUri([uri: '/test?param=demo']), '')
        tagLib.isLinkActiveFor(createTestLinkUri([uri: '/test/list/']), '')
        tagLib.isLinkActiveFor(createTestLinkUri([controller: 'test', action: 'list', params: [param: 'demo']]), '')

        when: "request uri is 'test/show'"
        request.forwardURI = "/test/show?page=2"
        then: "Not active for '/test/list' && '/testing/' && '/demo/test/'"
        !tagLib.isLinkActiveFor(createTestLinkUri([controller: 'test', action: 'list']), '')
        !tagLib.isLinkActiveFor(createTestLinkUri([uri: '/testing/']), '')
        !tagLib.isLinkActiveFor(createTestLinkUri([controller: 'demo', action: 'test']), 'default')
    }

    void "check active for include get params condition"() {

        when: "request uri is 'test/list?page=1&sort=desc'"
        request.forwardURI = "/test/list?page=1&sort=desc"
        then: "active for '/test/list?page=1&sort=desc' && '/test/list?sort=desc&page=1'"
        tagLib.isLinkActiveFor(createTestLinkUri([controller: 'test', action: 'list', params: [page: '1', sort: 'desc']]), 'include')
        tagLib.isLinkActiveFor(createTestLinkUri([uri: '/test/list?sort=desc&page=1']), 'include')
        
        when: "request uri is 'test/list?page=1&sort=desc'"
        request.forwardURI = "/test/list?page=1&sort=desc"
        then: "shouldn't be active for '/test/list?sort=asc&page=1' && '/test/list' && '/test/list?page=1'"
        !tagLib.isLinkActiveFor(createTestLinkUri([controller: 'test', action: 'list', params: [sort: 'asc', page: 1]]), 'include')
        !tagLib.isLinkActiveFor(createTestLinkUri([uri: '/test/list/']), 'include')
        !tagLib.isLinkActiveFor(createTestLinkUri([uri: '/test/list?page=1']), 'include')
    }

    def createTestLinkUri(attrs) {
        attrs['absolute'] = false
        tagLib.createLink(attrs)
    }
}
