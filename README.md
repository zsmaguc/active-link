# ActiveLinkTag Grails plugin v.1.0 #

This plugin is a wrapper over g:link tag. Plugin providing g:activeLink tag which will add additional css class for g:link according specified condition. It is a translation of RoR "active_link_to".

## Purpose ##
Easily mark g:link with extra css class which mean what link is active. Helpful if you are using navs components of bootstrap or foundation.

## Examples ##

### Simple ###
When forwardURI is '/books'

```
#!groovy

<g:activeLink controller="books" wrapper="li">Books</g:activeLink>
```
Result:

```
#!html

<li class="active"><a href="/books">Books</a></li>
```


### More examples ###

```
#!groovy

<g:activeLink controller="books" class="btn">Books</g:activeLink>
<g:activeLink controller="books" class="btn" wrapper="li">Books</g:activeLink>
<g:activeLink controller="books" class="btn" wrapper="li" activeClass="current">Books</g:activeLink>
<g:activeLink controller="manage" condition="[controller: ['books','authors']]">Manage</g:activeLink>
<g:activeLink controller="books" condition="${~/book/}">Books</g:activeLink>

```

Result:

```
#!html
<a href="/books" class="btn active>Books</a>
<li class="active"><a href="/books" class="btn">Books</a></li>
<li class="current"><a href="/books" class="btn">Books</a></li>
<li class="active"><a href="/manage">Manage</a></li>
<a href="/books" class="active>Books</a>
```


## Attributes ##

* *activeClass* (optional) - the name of css class for active link (By default: "active")
* *wrapper* (optional) - using for wrapping link in another tag. If link active the css class will be applied for wrapper.
* *condition* (optional) - custom rule for marking link as active.

### Condition allowed values ###


* Boolean
```
#!groovy
<g:activeLink controller="test" condition="${true}">Test</g:activeLink> == "<a href="/test" class="active">Test</a>"
```

* RegExp 
```
#!groovy
request.forwardURI == "/test/page/1"
<g:activeLink controller="demo" condition="${~/(\w)+\/(\w)+\/(\d)+/}">Test</g:activeLink> == "<a href="/demo" class="active">Test</a>"
```

* Map
```
#!groovy
<g:activeLink controller="demo" condition="[controller: 'test', action: 'list']">Test</g:activeLink>
<g:activeLink controller="demo" condition="[controller: ['test', 'demo']]">Test</g:activeLink>
```

* "exclude" will add active class if link href uri is equal to request.forward_URI excluding GET params. 
```
#!groovy
request.forwardURI == "/test/list?page=1"
<g:activeLink controller="test" action="list" params="[page: 2]" condition="exclude">Test</g:activeLink> == "<a href="/test/list?page=2" class="active">Test</a>"
```

* "include" take into consideration GET params. 
```
#!groovy
request.forwardURI == "/test/list?page=1"
<g:activeLink controller="test" action="list" params="[page: 2]" condition="include">Test</g:activeLink> == "<a href="/test/list?page=2">Test</a>"
```

About g:link attributes you can read in documentation. http://grails.org/doc/latest/ref/Tags/link.html
